package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.net.URL;
import java.sql.Time;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class Controller implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        boardSizeField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    boardSizeField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        snailsCountField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    boardSizeField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        board.getColumnConstraints().add(new ColumnConstraints(100* 1.0 / 4));
    }

    @FXML
    private void startAnimation(ActionEvent e) {
        stateChecker.setCycleCount(Timeline.INDEFINITE);
        stateChecker.play();
        grassGrower.setCycleCount(Timeline.INDEFINITE);
        grassGrower.play();
        eaten = 0;
        dead = 0;

        startSnails();
    }

    private void startSnails() {
        for(int i = 0 ; i < snails.length; i++) {
            snails[i].setCycleCount(Timeline.INDEFINITE);
            snails[i].play();
        }
    }

    @FXML
    private void initializeSnails(ActionEvent e) {

        snails = new Timeline[Integer.parseInt(snailsCountField.getText())];

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                grassGrower = new Timeline(new KeyFrame(Duration.millis(1), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        Rectangle rec = rectangles.get(rand.nextInt(boardSize)).get(rand.nextInt(boardSize));
                        if (rec.getFill().equals(Color.SANDYBROWN)) {
                            synchronized (rec) {
                                rec.setFill(Color.GREENYELLOW);
                            }
                        }
                    }
                }));

                stateChecker = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (dead == Integer.parseInt(snailsCountField.getText())) {
                            System.out.println("All is dead. Game over. There are " + eaten + " fields eaten.");
                            stateChecker.stop();
                        }
                    }
                }));
                for(int i = 0 ; i < Integer.parseInt(snailsCountField.getText()); i++) {
                    int finalI = i;
                    snails[i] = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            Rectangle rec = rectangles.get(rand.nextInt(boardSize)).get(rand.nextInt(boardSize));
                            if (rec.getFill().equals(Color.SANDYBROWN)) {
                                snails[finalI].stop();
                                dead++;
                                System.out.println("Umarłem " + finalI + " jako " + dead);
                            } else {
                                synchronized (rec) {
                                    rec.setFill(Color.SANDYBROWN);
                                    eaten++;
                                }
                            }
                        }
                    }));
                }
            }
        });
    }

    @FXML
    private void startButtonClicked(ActionEvent e) {
        clearBoard();
        initializeBoard(Integer.parseInt(boardSizeField.getText()));
    }

    private void clearBoard() {
        board.getChildren().clear();
        board.getColumnConstraints().clear();
        board.getRowConstraints().clear();
    }

    private void initializeBoard(int boardSize) {
        this.boardSize = boardSize ;
        createBoardConstraints(boardSize);

        rectangles = FXCollections.<ObservableList<Rectangle>>observableArrayList();
        for (int i = 0; i < boardSize; i++) {
            final ObservableList<Rectangle> row = FXCollections.<Rectangle>observableArrayList();
            rectangles.add(i, row);
            for (int j = 0; j < boardSize; j++) {
                row.add(new Rectangle(500/boardSize, 500/boardSize));
            }
        }


        for (int row = 0; row < boardSize; row++)
        {
            for (int col = 0; col < boardSize; col++) {
                Rectangle rec = rectangles.get(row).get(col);
                GridPane.setRowIndex(rec, row);
                GridPane.setColumnIndex(rec, col);
                board.getChildren().addAll(rec);
            }
        }
        fillRandom(boardSize);
        matrix = new SimpleListProperty<>();
        matrix.set(rectangles);
    }

    private void fillRandom(int boardSize) {
        for (int row = 0; row < boardSize; row++) {
            for (int col = 0; col < boardSize; col++) {
                rectangles.get(row).get(col).setFill(colors[rand.nextInt(2)]);
            }
        }
    }

    private void createBoardConstraints(int boardSize) {
        for (int row = 0; row < boardSize; row++)
        {
            board.getRowConstraints().add(new RowConstraints(500/boardSize));
        }
        for (int col = 0; col < boardSize; col++) {
            board.getColumnConstraints().add(new ColumnConstraints(500/boardSize));
        }
    }

    @FXML private GridPane board;
    @FXML private Label boardSizeLabel;
    @FXML private TextField boardSizeField;
    @FXML private TextField snailsCountField;
    private Color[] colors = new Color[]{Color.GREENYELLOW, Color.GREENYELLOW};
    private ObservableList<ObservableList<Rectangle>> rectangles;
    private Random rand = new Random();
    ListProperty<ObservableList<Rectangle>> matrix;
    private int boardSize;
    Timeline[] snails;
    Timeline grassGrower;
    private int dead = 0;
    private int eaten = 0;
    Timeline stateChecker;
}
