package sample;
import java.util.Random;

public class Grass {
    private GrassField[][] grass_field;

    public Grass(int number) {
        grass_field = new GrassField[number][number];
        for(int i = 0; i < grass_field.length; i++) {
            for(int j = 0; j < grass_field[i].length; j++) {
                grass_field[i][j] = new GrassField();
            }
        }
    }

    public void show() {
        for(int i = 0; i < grass_field.length; i++) {
            for(int j = 0; j < grass_field[i].length; j++) {
                System.out.print(grass_field[i][j] + " ");
            }
            System.out.println();
        }
    }

    public GrassField[][] getGrass_field() {
        return grass_field;
    }

    public void setGrass_field(GrassField[][] grass_field) {
        this.grass_field = grass_field;
    }
}
