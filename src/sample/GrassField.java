package sample;
import java.util.Random;

public class GrassField {
    private int field = 0;

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    public GrassField() {
        Random random = new Random();
        this.field = random.nextInt(2);
    }
    public void Decrement() {
        field --;
    }
    public void Increment() {
        field++;
    }

    @Override
    public String toString() {
        return Integer.toString(field);
    }
}
