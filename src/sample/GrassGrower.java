package sample;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GrassGrower implements Runnable{
    private Grass common_grass;
    private Thread t;
    private void eat() {
        while (true) {
            Random random = new Random();
            GrassField[][] grass_field = common_grass.getGrass_field();
            GrassField got_field = grass_field[random.nextInt(grass_field.length)][random.nextInt(grass_field.length)];
            synchronized (got_field) {
                got_field.Increment();
            }
            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public GrassGrower(Grass grass) {
        common_grass = grass;
    }

    @Override
    public void run() {
        eat();
    }

    public void start () {
        if (t == null) {
            t = new Thread (this);
            t.start ();
        }
    }
    public Boolean isAlive() {
        return t.isAlive();
    }
}
